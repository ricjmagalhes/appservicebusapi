﻿using Microsoft.ServiceBus.Messaging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ConsoleAppServiceBusAPI
{
    class Program
    { 

        static void Main(string[] args)
        {
            string connectionStringServiceBusQueue = "****";
            string queueName = "salesorderin";
  
            Dictionary<string, string> dic = new Dictionary<string, string>();
            dic.Add("MessageTypeNamespace", "XXXManager_Services.Class");
            //dic.Add("MessageTypeName", "SaleOrderIntegrated");
            dic.Add("MessageTypeFullName", "XXXManager_Services.Class.ContractUpdate");
            dic.Add("MessageTypeName", "ContractUpdate");

            string msg2 = @"{
                  ""requestId"": ""20072110000174"",
                  ""contractNumber"": ""36200001790"",
                  ""productCode"": ""50""
                }";


            SendToQueue(msg2, dic, connectionStringServiceBusQueue, queueName);

            Console.WriteLine("Hello World!");
        }
         

        static public void SendToQueue(string msg, Dictionary<string, string> properties, string connectionStringServiceBus, string queueOrTopicName)
        {    //Queue Client
                QueueClient client = QueueClient.CreateFromConnectionString(connectionStringServiceBus, queueOrTopicName);

                //request Data
                byte[] requestData = Encoding.UTF8.GetBytes(msg);
                Stream stream = new MemoryStream(requestData);
                BrokeredMessage message = new BrokeredMessage(stream);

            Guid MessageId = new Guid();

                //Set message identifier
                message.MessageId = MessageId.ToString();

                //Iterate Dictionary
                foreach (KeyValuePair<string, string> prop in properties)
                    message.Properties.Add(prop.Key, prop.Value);

                //Send Message
                client.Send(message);
            
        }

        public void MssSenderTopic(string ssMessage, string ssTopicName, string ssConnectionString, List<string> ssProperties, int ssScheduledDelaySeconds, out string ssResult)
        {
            //request Data
            byte[] requestData = Encoding.UTF8.GetBytes(ssMessage);

            //OutSystems.HubEdition.RuntimePlatform.Log.GeneralLog.StaticWrite(
            //        DateTime.Now
            //        , AppInfo.GetAppInfo().OsContext.Session.SessionID
            //        , AppInfo.GetAppInfo().eSpaceId
            //        , AppInfo.GetAppInfo().Tenant.Id
            //        , AppInfo.GetAppInfo().OsContext.Session.UserId
            //        , "INIT MssSenderTopic ssTopicName: " + ssTopicName + " | ssMessage: " + ssMessage + " | ssScheduledDelaySeconds: " + ssScheduledDelaySeconds
            //        , "Information"
            //        , AppInfo.GetAppInfo().ApplicationName
            //        , ""
            //    );

            try
            {
                Stream stream = new MemoryStream(requestData);
                BrokeredMessage message = new BrokeredMessage(stream);

                //Topic Client
                TopicClient client = TopicClient.CreateFromConnectionString(ssConnectionString, ssTopicName);

                //Set message identifier
                message.MessageId = Guid.NewGuid().ToString();
                message.ScheduledEnqueueTimeUtc = DateTime.UtcNow.AddSeconds(ssScheduledDelaySeconds);

                if (ssProperties.Count > 0)
                {

                    foreach (string prop in ssProperties)
                    {
                        message.Properties.Add("", "");
                    }

                }

                //Send Message
                client.Send(message);
                ssResult = "SUCCESS";

            }
            catch (Exception e)
            {
                //OutSystems.HubEdition.RuntimePlatform.Log.ErrorLog.StaticWrite(
                //                DateTime.Now
                //                , AppInfo.GetAppInfo().OsContext.Session.SessionID
                //                , AppInfo.GetAppInfo().eSpaceId
                //                , AppInfo.GetAppInfo().Tenant.Id
                //                , AppInfo.GetAppInfo().OsContext.Session.UserId
                //                , e.Message
                //                , e.ToString()
                //                , "MssSenderTopic"
                //                );
                ssResult = "ERROR | " + e.Message;
                throw e;
            }
        } // MssSenderTopic


    }
}
